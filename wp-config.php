<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'trident_assistance');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ')wGzGb&eHH-g;f@Ai^eA] 6c.oDtjSY;D2;s;|-BQX=f{ [!pn~WIPq:3OZ?SgN|');
define('SECURE_AUTH_KEY',  'wI&MLiyzC1,?Zv~5*T 1{YvkAmpOb*AMkc@Yax]iZ`*7)D9Eg5ID^3;[~p8/a+`j');
define('LOGGED_IN_KEY',    '0pB,E<5pW:.S~=kd5F;<CKWQNg,>[rJ2$mt=Sj|q$K!yITHr:j&%?WM]ktMF;}NJ');
define('NONCE_KEY',        '0dzix!UtLH6xqd&w|b`hR_5h+_jt[ `Fb:nql##UY|5~HHNFrnKI)mIPNo+xY5S9');
define('AUTH_SALT',        'F%Tj.!f{S$?1 ,AK4PwSse+-B@uj9UV&bnU~,5:4*SUGiJ+1`Uck7-LQuW~EYrFy');
define('SECURE_AUTH_SALT', 'fzw]14SaFM[I,e|=_G ,+Hn$^7=(}6r?qj9nQ4fu{@(,lmGvAK;?hh#|Cp]_,j&J');
define('LOGGED_IN_SALT',   'cc+  Yt<|,+N)q*)>4^ZrNcQB&xnz^+vXI8$ ~t)hvL~,|RX(u;q8Uoh@!Xve$wm');
define('NONCE_SALT',       '>.8WI<i{`FdL]zX5fTtB|.{Q`z=!P^AL/(~Ce$1MLkb<(bPYAnMH;zP?M55I61NC');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
