<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Trident_Assistance
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="ip-container" class="ip-container">
			<!-- initial header -->
			<header class="ip-header">
				<div class="ip-loader">
					<img class="loader-logo_icon" src="<?php echo get_template_directory_uri(); ?>/img/layout/loader-logo.svg">
					<svg class="ip-inner" width="60px" height="60px" viewBox="0 0 80 80">
						<path class="ip-loader-circlebg" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
						<path id="ip-loader-circle" class="ip-loader-circle" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
					</svg>
				</div>
			</header>
		</div>

		<div class="hero">
			<button id="nav-toggle" class="offcanvas-toggle js-offcanvas-toggle"><span></span></button>
			<div class="fullscreen-bg">
				<video class="fullscreen-bg_video" loop muted autoplay>
				<source src="<?php echo get_template_directory_uri(); ?>/video/video.mp4" type="video/mp4">
				<source src="<?php echo get_template_directory_uri(); ?>/video/video.ogg" type="video/ogg">
					Your browser does not support the video tag.
				</video><!--./fullscreen-bg_video-->
			</div><!--./fullscreen-bg-->
			<div class="container">
				<div class="row  parallax-module__wrapper parallax-module">
					<div class="col-xs-12">
						<div class="align-container">
							<div class="align-inner">
								<div class="col-sm-6">
									<div class="parallax-module__image">
										<img class="hero-logo_icon" src="<?php echo get_template_directory_uri(); ?>/img/content/logo.svg">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="parallax-module__content">
										<h2 class="hero-title"> Trident Assistance </h2>
										<h3 class="hero-content"> <strong> Bold vision. </strong> Smart execution.</h3>
									</div>
								</div><!--./8-->
							</div><!--./aligninner-->
							<div class="hero-buttons">
								<a href="" class="button-buy button-blue"> Buy now</a>
								<a href="" class="button-buy button-white"> See all</a>
							</div><!--./hero-button-->

						</div><!--./aligncontainer-->
					</div><!--./12-->
				</div><!--./row-->
			</div><!--./container-->
		</div><!--./hero-->
		<div class="offcanvas">
			<div class="container-fluid">
				<div class="row no-gutter">
					<div class="col-sm-10">
						<div class="mobile-wrap">
							<ul class="nav--mobile">
								<?php wp_nav_menu( array( 'container' => '', 'menu_id' => 'primary-menu', 'menu_class'=> 'nav--mobile' ) ); ?>
							</ul><!-- /.nav-mobile -->
						</div><!-- /.mobile-wrap -->
					</div><!-- /.8 -->
					<div class="col-sm-2">
						<div class="social-wrap">
							<ul class="icons--mobile">
								<li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/img/icons/mail.png" alt="Trident Assistance"></a></li>
								<li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/img/icons/face.png" alt="Trident Assistance"></a></li>
								<li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/img/icons/inst.png" alt="Trident Assistance"></a></li>
								<li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/img/icons/youtube.png" alt="Trident Assistance"></a></li>
							</ul>
							<p class="rotator"> Trident Assistance </p>
						</div><!-- /.social-wrap -->
					</div><!-- /.2 -->
				</div><!-- /.row -->
			</div><!-- /.container -->
		</div><!-- /.offcanvas -->
		<header class="site-header site-header--sticky js-sticky-header">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12">
						<a href="" class="site-header__logo">
							<img class="hero-logo" src="<?php echo get_template_directory_uri(); ?>/img/content/logo.svg" alt="Trident Assistance">
						</a><!-- /.site-header__logo -->
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.container -->
		</header><!-- /.site-header -->
