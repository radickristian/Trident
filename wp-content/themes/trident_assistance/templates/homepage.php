<?php
/*
Template Name: Homepage
*/

get_header();
?>
		<?php while ( have_posts() ) : the_post(); ?>

			   <div class="slogan">
		   <div class="container">
				<div class="row">
					<div class="align-container">
						<div class="align-inner">
							<div class="col-sm-12">
								<h3> 
									We build strong brands and grow businesses through branded services, digital storytelling, engaging content and rich shopping experiences.
								</h3>
								<img src="img/layout/slogan.png" alt="Trident Assistance Slogan">
							</div><!-- /.12 -->
						 </div><!--./aligninner-->
					</div><!--./aligncontainer-->
				</div><!-- /.row -->
			</div><!-- /.container -->
		</div><!-- /.slogan -->

		<div class="prons" style="background-image: url('img/content/prons.jpg');">
		   <div class="container">
				<div class="row">
					<div class="align-container">
						<div class="align-inner">
							<div class="col-sm-12">
								<span> BOOK DIRECT </span>
								<h3> Trident Community Benefits </h3>
								<p>
									Book now and become a part of the trident community. <br> Instantly enjoy 10% off your stay, along with exclusive access  to our curated city guides. 
									<br> 
									<br>
									You are being treated like a royal, and with security on top level.
								</p>
								<a><img src="img/icons/royal.png"></a>
								<a><img src="img/icons/map.png"></a>
								<a><img src="img/icons/lock.png"></a>
							</div><!-- /.12 -->
						 </div><!--./aligninner-->
					</div><!--./aligncontainer-->
				</div><!-- /.row -->
			</div><!-- /.container -->
		</div><!-- /.prons -->
		<div class="destionation-wrap">
		   <div class="container">
			   <div class="slick">
					<div class="row no-gutter">
						<div class="col-sm-5">
							<div class="destination-image" style="background-image: url('img/content/zipline_v.jpg');">
							</div>
						</div> <!--./4-->
						<div class="col-sm-7">
							<div class="destination-content">
								<span> Destinations </span>
								<h3> Zippline Croatia </h3>
								<p>
									How does the idea of hanging of a 150 meters high steel wire above the beautiful canyon of the river Cetina sounds like?
								</p>
								<a href="#">
									Find more about
								</a>
							</div><!--./destination-->
							 <div class="destination-image destination-image-sm" style="background-image: url('img/content/zipline_h.jpg');">
							</div>
						</div><!--./8-->
					</div><!-- /.row -->
					<div class="row no-gutter">
						<div class="col-sm-5">
							<div class="destination-image" style="background-image: url('img/content/riva_v.jpg');">
							</div>
						</div> <!--./4-->
						<div class="col-sm-7">
							<div class="destination-content">
								<span> Destinations </span>
								<h3> Split - Riva </h3>
								<p>
									How does the idea of hanging of a 150 meters high steel wire above the beautiful canyon of the river Cetina sounds like?
								</p>
								<a href="#">
									Find more about
								</a>
							</div><!--./destination-->
							<div class="destination-image destination-image-sm" style="background-image: url('img/content/riva_h.jpg');">
							</div>
						</div><!--./8-->
					</div><!-- /.row -->
				</div><!--slick-->
			</div><!-- /.container -->
		</div><!-- /.prons -->

		<div class="info">
		   <div class="container">
				<div class="row">
					<div class="col-sm-6">
						<span> Split Spirit </span>
						<h3> 
							Welcome to the <br> potato 
						</h3>
					</div><!-- /.6 -->
					<div class="col-sm-6">
						<p>
							At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident.
						</p>
					</div><!-- /.6 -->
				</div><!-- /.row -->
			</div><!-- /.container -->
		</div><!-- /.slogan -->

		<div class="info">
		   <div class="container-fluid">
				<div class="row no-gutter">
					<div class="col-sm-7">
						<div class="left-banner" style="background-image: url('img/content/banner_1.jpg');">
						</div>
					</div><!-- /.7 -->
					<div class="col-sm-5">
					   <div class="right-banner" style="background-image: url('img/content/banner_2.jpg');">
					   </div>
					   <div class="right-banner" style="background-image: url('img/content/banner_3.jpg');">
					   </div>
					</div><!-- /.5 -->
				</div><!-- /.row -->
			</div><!-- /.container -->
		</div><!-- /.slogan -->

		 <div class="home-post">
		   <div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h3> View top of </h3>
						 <ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class=""><a href="#Land" aria-controls="Land" role="tab" data-toggle="tab"><img src="img/icons/air.png">Land</a></li>
							<li role="presentation"><a href="#Sea" aria-controls="Sea" role="tab" data-toggle="tab"><img src="img/icons/sea.png">Sea</a></li>
							<li role="presentation"><a href="#Air" aria-controls="Air" role="tab" data-toggle="tab"><img src="img/icons/land.png">Air</a></li>
						</ul>
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane fade in active" id="Land">
								<div class="col-sm-6">
									<div class="home-post_item">
										<div class="home-post_item--top" style="background-image: url(img/content/banner_3.jpg);" >
										</div>
										<div class="home-post_item--bottom">
											<em> 27.5.2017 </em>
											<span class="hashs"> #Konferencija / #Budučnost </span>
											<a> Održana Završna konferencija: <br>Projekt ForestEye</a>
										</div><!-- /.bottom -->
									</div><!-- /.item -->
								</div><!-- /.6 -->
								<div class="col-sm-6">
									<div class="home-post_item">
										<div class="home-post_item--top" style="background-image: url(img/content/banner_3.jpg);" >
										</div>
										<div class="home-post_item--bottom">
											<em> 27.5.2017 </em>
											<span class="hashs"> #Konferencija / #Budučnost </span>
											<a> Održana Završna konferencija: <br>Projekt ForestEye</a>
										</div><!-- /.bottom -->
									</div><!-- /.item -->
								</div><!-- /.6 -->
							</div><!-- /.tabpanel -->
							<div role="tabpanel" class="tab-pane fade" id="Sea">
								<div class="col-sm-6">
									<div class="home-post_item">
										<div class="home-post_item--top" style="background-image: url(img/content/riva_h.jpg);" >
										</div>
										<div class="home-post_item--bottom">
											<em> 27.5.2017 </em>
											<span class="hashs"> #Konferencija / #Budučnost </span>
											<a> Održana Završna konferencija: <br>Projekt ForestEye</a>
										</div><!-- /.bottom -->
									</div><!-- /.item -->
								</div><!-- /.6 -->
								<div class="col-sm-6">
									<div class="home-post_item">
										<div class="home-post_item--top" style="background-image: url(img/content/riva_h.jpg);" >
										</div>
										<div class="home-post_item--bottom">
											<em> 27.5.2017 </em>
											<span class="hashs"> #Konferencija / #Budučnost </span>
											<a> Održana Završna konferencija: <br>Projekt ForestEye</a>
										</div><!-- /.bottom -->
									</div><!-- /.item -->
								</div><!-- /.6 -->
							</div><!-- /.tabpanel -->
							<div role="tabpanel" class="tab-pane fade" id="Air">
								<div class="col-sm-6">
									<div class="home-post_item">
										<div class="home-post_item--top" style="background-image: url(img/content/banner_2.jpg);" >
										</div>
										<div class="home-post_item--bottom">
											<em> 27.5.2017 </em>
											<span class="hashs"> #Konferencija / #Budučnost </span>
											<a> Održana Završna konferencija: <br>Projekt ForestEye</a>
										</div><!-- /.bottom -->
									</div><!-- /.item -->
								</div><!-- /.6 -->
								<div class="col-sm-6">
									<div class="home-post_item">
										<div class="home-post_item--top" style="background-image: url(img/content/banner_2.jpg);" >
										</div>
										<div class="home-post_item--bottom">
											<em> 27.5.2017 </em>
											<span class="hashs"> #Konferencija / #Budučnost </span>
											<a> Održana Završna konferencija: <br>Projekt ForestEye</a>
										</div><!-- /.bottom -->
									</div><!-- /.item -->
								</div><!-- /.6 -->
							</div><!-- /.tabpanel -->
						</div><!-- /.tabcontent -->
					 </div><!-- /.12 -->   
				</div><!-- /.row -->
			</div><!-- /.container -->
		</div><!-- /.slogan -->

		<?php endwhile; ?>
<?php
get_footer();
